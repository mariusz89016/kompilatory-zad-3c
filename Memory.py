class FunctionSymbol:
    def __init__(self, type, args_list, compound_instr):
        self.type = type
        self.args_list = args_list
        self.compound_instr = compound_instr


class Memory:
    def __init__(self, name, initial_map=None):  # memory name
        if not initial_map:
            initial_map = {}
        self.name = name
        self.map = initial_map

    def get(self, name):  # get from memory current value of variable <name>
        if name in self.map:
            return self.map[name]
        return None

    def put(self, name, value):  # puts into memory current value of variable <name>
        self.map[name] = value

    def __contains__(self, item):
        return item in self.map

    def __getitem__(self, item):
        return self.map[item]

    def __setitem__(self, key, value):
        self.map[key] = value


# przegladamy wszystko od poczatku do konca
class MemoryStack:
    def __init__(self, memory=None):  # initialize memory stack with memory <memory>
        self.stack = [memory]

    def get(self, name):  # get from memory stack current value of variable <name>
        for memory in self.stack[::-1]:
            if name in memory:
                return memory.get(name)
        return None

    def insert(self, name, value):  # inserts into memory stack variable <name> with value <value>
        self.stack[-1].put(name, value)

    def set(self, name, value):  # sets variable <name> to value <value>
        for memory in self.stack[::-1]:
            if name in memory:
                memory[name] = value
                return
        raise Exception("Value not set")

    def push(self, memory):  # push memory <memory> onto the stack
        self.stack.append(memory)

    def pop(self):  # pops the top memory from the stack
        self.stack.pop()


# przegladamy tylko to co lezy na wierzcholku stosu lub to co jest na samym dole (czyli zmienne globalne)
class FunctionStack:
    def __init__(self, memory=None):
        self.stack = [memory]

    def get(self, name):
        if self.stack[-1].get(name) is not None:
            return self.stack[-1].get(name)
        elif self.stack[0].get(name) is not None:
            return self.stack[0].get(name)
        else:
            raise Exception("Problem")

    def insert(self, name, value):
        self.stack[-1].insert(name, value)

    def set(self, name, value):
        for memory in self.stack[-1].stack:
            if name in memory:
                self.stack[-1].set(name, value)
                return
        for memory in self.stack[0].stack:
            if name in memory:
                self.stack[0].set(name, value)
                return
        raise Exception("Could not set!")

    def push(self, memory):
        self.stack.append(memory)

    def pop(self):
        self.stack.pop()
