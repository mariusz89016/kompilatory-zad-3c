import pprint
import AST
import SymbolTable
from Memory import *
from Exceptions import *
from visit import *


functionStack = FunctionStack(memory=MemoryStack(memory=Memory("global_scope")))


class Interpreter(object):
    @on('node')
    def visit(self, node):
        print "@on('node')"

    @when(AST.Program)
    def visit(self, node):
        for decl in node.declarations.list:
            decl.accept2(self)
        for fundef in node.fundefs.list:
            fundef.accept2(self)
        for instr in node.instructions.list:
            instr.accept2(self)

    @when(AST.Declaration)
    def visit(self, node):
        for init in node.inits.list:
            init.accept2(self)

    @when(AST.Init)
    def visit(self, node):
        value = node.expression.accept2(self)
        functionStack.stack[-1].insert(node.id.const, value)

    @when(AST.BinaryExpression)
    def visit(self, node):
        r1 = node.left.accept2(self)
        r2 = node.right.accept2(self)
        if isinstance(r1, str):
            r1 = "\""+r1+"\""
        if isinstance(r2, str):
            r2 = "\""+r2+"\""
        return eval("{0} {1} {2}".format(r1, node.op, r2))

    @when(AST.Assignment)
    def visit(self, node):
        value = node.expression.accept2(self)
        functionStack.set(node.id.const, value)

    @when(AST.Integer)
    def visit(self, node):
        return int(node.const)

    @when(AST.Float)
    def visit(self, node):
        return float(node.const)

    @when(AST.String)
    def visit(self, node):
        return node.const[1:-1]

    @when(AST.Id)
    def visit(self, node):
        return functionStack.get(node.const)

    @when(AST.WhileInstruction)
    def visit(self, node):
        try:
            r = None
            while node.condition.accept2(self):
                try:
                    r = node.instruction.accept2(self)
                except ContinueException:
                    pass
            return r
        except BreakException:
            pass

    @when(AST.RepeatInstruction)
    def visit(self, node):
        try:
            for instr in node.instructions.list:
                instr.accept2(self)
        except ContinueException:
            pass

        while not node.condition.accept2(self):
            try:
                for instr in node.instructions.list:
                    instr.accept2(self)
            except ContinueException:
                pass

    @when(AST.PrintInstruction)
    def visit(self, node):
        output = node.expression.accept2(self)
        print output

    @when(AST.Condition)
    def visit(self, node):
        value = node.expression.accept2(self)
        return value

    @when(AST.CompoundInstruction)
    def visit(self, node):
        try:
            functionStack.stack[-1].push(Memory("compound_instruction"))
            for decl in node.declarations.list:
                decl.accept2(self)
            for instr in node.instructions.list:
                instr.accept2(self)
        finally:
            functionStack.stack[-1].pop()

    @when(AST.ChoiceInstructionIf)
    def visit(self, node):
        if node.condition.accept2(self):
            node.if_instruction.accept2(self)

    @when(AST.ChoiceInstructionIfElse)
    def visit(self, node):
        if node.condition.accept2(self):
            node.if_instruction.accept2(self)
        else:
            node.else_instruction.accept2(self)

    @when(AST.ParenthesisExpression)
    def visit(self, node):
        return node.expression.accept2(self)

    @when(AST.BreakInstruction)
    def visit(self, node):
        raise BreakException()

    @when(AST.ContinueInstruction)
    def visit(self, node):
        raise ContinueException()

    @when(AST.ReturnInstruction)
    def visit(self, node):
        output = node.expression.accept2(self)
        raise ReturnValueException(output)

    @when(AST.Fundef)
    def visit(self, node):
        functionStack.stack[-1].insert(node.id.const, FunctionSymbol(node.type, node.args_list.list, node.compound_instr))

    @when(AST.FunctionCall)
    def visit(self, node):
        try:
            arguments_map = {}
            args_list = functionStack.get(node.id).args_list
            for argument, expression in zip(args_list, node.instruction.list):
                arguments_map[argument.id.const] = expression.accept2(self)

            memory = Memory("function_scope " + node.id, initial_map=arguments_map)
            functionStack.push(MemoryStack(memory=memory))

            compound_instr = functionStack.get(node.id).compound_instr
            compound_instr.accept2(self)
        except ReturnValueException as returnValue:
            return returnValue.value
        finally:
            functionStack.pop()